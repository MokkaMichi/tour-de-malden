/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Tracks time for individual teams.
*
*	Parameters:
*		0: Mode (STRING, see above)
*		1: Team name (STRING)
*		2: Arguments (ANYTHING)
*/
#define SCRIPT fn_trackTime
#include "..\..\common.hpp"
#include "macros.hpp"

//run_servercheck;

_teams = [team_1,team_2,team_3,team_4,team_5,team_6,team_7];

params [
	["_mode","init",[""]],
	["_team",objNull,[objNull]],
	["_args",[],[[]]]
];


_mode = tolower _mode;

switch (_mode) do {
	case "init": { 			// Used to initialise the system. Starts the display loop

		{
			_varName = format ["StartTime_%1",_x];
			missionNamespace setVariable [_varName,0,true];

			_varName = format ["formattedCOV_%1",_x];
			missionNamespace setVariable [_varName,"Race has not been started.",true];

			_varName = format ["formattedCS_%1",_x];
			missionNamespace setVariable [_varName,"Race has not been started.",true];
		} foreach _teams;
		
		raceStarted = false;

		/****************************/
		/*		USER SETTINGS		*/
		/****************************/

		totalLaps = 3;		// Total number of laps, 1-based
		checkPoints = [		// All checkpoints by name
			"cp_alpha",
			"cp_bravo",
			"cp_charlie",
			"cp_delta"
		];

		/****************************/
		/*	  END USER SETTINGS  	*/
		/****************************/

		/*
		while {true} do {
			["displayTeam",team_1] call LR_fnc_trackTime;
			sleep 0.1;
		};
		*/
	};

	case "display": {		// Displays the current times for every team.

	_args params [
		["_clientID",0,[0]]
	];

		if (not raceStarted) exitwith {["Race has not been started yet."] remoteExec ["hintSilent",0];};
		_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br />Team 1: %8/%15 - %1<br />Team 2: %9/%15 - %2<br />Team 3: %10/%15 - %3<br />Team 4: %11/%15 - %4<br />Team 5: %12/%15 - %5<br />Team 6: %13/%15 - %6<br />Team 7: %14/%15 - %7";

		GETVAR(team_1);
		GETVAR(team_2);
		GETVAR(team_3);
		GETVAR(team_4);
		GETVAR(team_5);
		GETVAR(team_6);
		GETVAR(team_7);

		_formString = parseText format [_body,_team_1,_team_2,_team_3,_team_4,_team_5,_team_6,_team_7,_lap_team_1,_lap_team_2,_lap_team_3,_lap_team_4,_lap_team_5,_lap_team_6,_lap_team_7,totalLaps];
		[_formString] remoteExec ["hintSilent",_clientID];
	};

	case "registerteam": {	// Registers a team to participate in the race, starting the timer.

		if (isNull _team) exitWith {
			_msg = format ["Supplied team to be registered must not be NULL"];
			logerror (_msg);
		};

		if (not (_team in _teams)) exitwith {
			_msg = format ["Invalid team object supplied: %1",_team];
			logerror(_msg);
		};

		_startTime = serverTime;

		_msg = format ["Starting at %1 with Team %2",_startTime,_team];
		loglog(_msg);

		_varName = format ["StartTime_%1",_team];
		missionNamespace setVariable [_varName,_startTime,true];

		_varName = format ["lap_%1",_team];
		missionNamespace setVariable [_varName,1,true];

		_varName = format ["registered_%1",_team];
		missionNamespace setVariable [_varName,true,true];
	};

	case "checkpoint": {	// Handles checkpoints.
		if ((count _args) < 1) exitWith {
			_msg = "Current checkpoint must be supplied.";
			logerror(_msg);
		};

		_args params [
			["_checkpoint","cp_alpha",[""]]
		];

		_msg = format ["Checkpoint: %1",_checkpoint];
		loglog(_msg);

		_varName = format ["StartTime_%1",_team];
		_passTime = [missionNamespace getVariable [_varName,0]] call LR_fnc_deltaTime;

		_varName = format ["lap_%1",_team];
		_passLap = missionnamespace getVariable [_varName,0];

		_varName = format ["lap%1_%2_passTime_%3",_passLap,_checkpoint,_team];
		missionnamespace setVariable [_varName,_passTime,true];

		_msg = format ["passT: %1 | passL: %2 | varName: %3",_passTime,_passLap,_varName];
		loglog(_msg);

		_sID = _team call LR_fnc_getTeamID;
		_cpID = _checkpoint call LR_fnc_getCheckPointID;

		_passTime = _passTime call LR_fnc_convertTime;

		_msg = format ["Team %1 has passed Checkpoint %2 in lap %3/%4 in %5.",(_sID + 1),_cpID,_passLap,totalLaps,_passTime];
		[_msg] remoteExec ["BIS_fnc_titleText",0];
	};

	case "displayteam": {	// Displays time stats for only one team

		_varName = format ["raceCompleted_%1",_team];
		_completed = missionNamespace getVariable [_varName,false];
		if (_completed) then {
			_body = "<t color='#ff0000' size='2.5' align='center'>Race Completed</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Race completed after: %9</t>";
		} else {
			_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Current Time: %2<br /><br />Current Lap: %3/%4<br />Checkpoint Alpha: %5<br />Checkpoint Bravo: %6<br />Checkpoint Charlie: %7<br />Checkpoint Delta: %8</t>";
		};

		_varName = format ["StartTime_%1",_team];
		_sTime = missionnamespace getVariable [_varName,0];

		_varName = format ["lap_%1",_team];
		_sLap = missionnamespace getVariable [_varName,0];

		_sID = _team call LR_fnc_getTeamID;

		_varR = format["registered_%1",_team];
		_reg = missionNamespace getVariable [_varR,false];

		_dTime = _sTime call LR_fnc_deltaTime;
		_dTime = _dTime call LR_fnc_convertTime;
		if ((_sTime == 0) || (not _reg)) then { _dTime = "Not started."; };

		CPVAR(cp_alpha);
		CPVAR(cp_bravo);
		CPVAR(cp_charlie);
		CPVAR(cp_delta);

		_varName = format  ["raceCompletedTime_%1",_team];
		_rcT= missionnamespace getVariable [_varName,0];

		_formString = parseText format [_body,(_sID + 1),_dTime,_sLap,totalLaps,_cp_alpha,_cp_bravo,_cp_charlie,_cp_delta,_rcT];
		[_formString] remoteExec ["hintSilent",0,true];
	};

	case "lap": {			// Registers a completed lap.
		_args params [
			["_curLap",0,[0]]
		];

		_curCp = [_team,1] call LR_fnc_getCheckpoints;
		_curCp = _curCp select 0;

		if (_curCp != "cp_delta") exitWith {
			["You have to pass all checkpoints before completing a lap!"] call LR_fnc_sendTeamMessage;
		};

		_body = "";

		_newLap = _curLap + 1;
		if (_newLap > totalLaps) then {
			_varName = format["raceCompleted_%1",_team];
			missionNamespace setVariable [_varName,true,true];

			_varName = format ["StartTime_%1",_team];
			_sTime = missionnamespace getVariable [_varName,0];
			_dTime = _sTime call LR_fnc_deltaTime;

			_varName = format["raceCompletedTime_%1",_team];
			missionNamespace setVariable [_varName,_dTime,true];
		};

		_varName = format ["StartTime_%1",_team];
		_passTime = [missionNamespace getVariable [_varName,0]] call LR_fnc_deltaTime;

		_fTime = _passTime call LR_fnc_convertTime;

		_varName = format ["lap%2_passtime_%1",_x];
		missionnamespace setVariable [_varName,_passTime,true];

		_varName = format ["lap_%1",_x];
		missionnamespace setVariable [_varName,_newLap,true];

		_sID = _team call LR_fnc_getTeamID;

//		{
//			_varName = format ["lap%1_%2_passTime_%3",_new,_x,_team];
//			missionnamespace setVariable [_varName,0,true];
//		} forEach ["cp_alpha","cp_bravo","cp_charlie","cp_delta"];

		_msg = format ["Team %1 has completed lap %2/%3 in %4.",(_sID + 1),_curLap,totalLaps,_fTime];
		[_msg] remoteExec ["BIS_fnc_titleText",0]
	};

	case "resetTeam": {	// Resets the time splitter for the given team.
		if (isNull _team) exitWith {
			_msg = format ["Supplied team to be registered must not be NULL"];
			logerror (_msg);
		};

		if (not (_team in _teams)) exitWith {
			_msg = format ["Invalid team object supplied: %1",_team];
			logerror(_msg);
		};

		_msg = format ["Resetting time for %1",_team];
		loglog(_msg);

		_varName = format ["registered_%1",_team];
		missionNamespace setVariable [_varName,false,true];

		_varName = format ["StartTime_%1",_team];
		missionNamespace setVariable [_varName,0,true];

		_varName = format ["lap_%1",_team];
		missionNamespace setVariable [_varName,1,true];
	};

	case "reset": {			// Resets time splitters for all teams.
		{
			 ["resetTeam",_x] call LR_fnc_trackTime;
		} forEach _teams;

		_msg = "Time splitters reset for all teams.";
		loglog(_msg);

		raceStarted = false;
	};

	case "gettime": {		// Returns the formatted time overview as an array

		_args params [
			["_player",objNull,[objNull]]
		];

		_msg = format ["gettime | args: %1",_player];
		loglog(_msg);
		
		GETVAR(team_1);
		GETVAR(team_2);
		GETVAR(team_3);
		GETVAR(team_4);
		GETVAR(team_5);
		GETVAR(team_6);
		GETVAR(team_7);

		_return = [_team_1,_team_2,_team_3,_team_4,_team_5,_team_6,_team_7,_lap_team_1,_lap_team_2,_lap_team_3,_lap_team_4,_lap_team_5,_lap_team_6,_lap_team_7,totalLaps];

		_msg = format ["gettime | return: %1",_return];
		loglog(_msg);

		_player setVariable ["rawTimeBuffer",_return,true];
	};
};