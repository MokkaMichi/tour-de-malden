/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Handles trigger actions for clients.
*		Mainly broadcasting times and stuff...
*
*	Parameters:
*		0: Trigger array (ARRAY)
*		1: Mode setting (STRING)
*
*	Return Value:
*		Nothing.
*/
#define SCRIPT fn_handleClientTrigger
#include "..\..\common.hpp"
#include "macros.hpp"

//run_servercheck;

params [
	["_thisList",[],[[]]],
	["_mode","start",[""]]
];

_teams = [team_1,team_2,team_3,team_4,team_5,team_6,team_7];

_players = [];
_completeCrew = [];

_registered 	= [];
_Nregistered	= [];

{
	_tVarName 	= format ["registered_%1",_x];
	_tVar		= missionnamespace getVariable [_tVarName,false];

	_msg = format["tVar: %1",_tVar];			// debug
	loglog(_msg);								// debug

	_msg = format["_tVarName: %1",_tVarName];	// debug
	loglog(_msg);	
								// debug
	if (_tVar) then {

		_registered 	pushBack _x;

		_msg = format["_x reg: %1",_x];			// debug
		loglog(_msg);							// debug

		} else {

		_Nregistered 	pushBack _x;

		_msg = format["_x nReg: %1",_x];		// debug
		loglog(_msg);							// debug
	};
} forEach _thisList;

_msg = format ["thisList: %1",_thisList];		// debug
loglog(_msg);							// debug

{
	_completeCrew append (crew _x);
	_msg = format ["cC _x: %1",_thisList];		// debug
	loglog(_msg);							// debug

	{
		_reg = _x getVariable ["registered","-1"];
		if ((parseNumber _reg) == -1) then {
			_x setVariable ["registered",false];
		};
	} forEach (crew _x);

	
} forEach _thisList;

_msg = format ["completeCrew: %1",_completeCrew];		// debug
loglog(_msg);							// debug



{
	if (_x in playableUnits) then {
		_players pushBack _x;
	};
} forEach _completeCrew;

_msg = format ["Players: %1",_players];		// debug
loglog(_msg);							// debug



_mode = tolower _mode;

_msg = format ["Mode Setting: %1",_mode];
loglog(_msg);

switch (_mode) do {
	case "broadcast": {
		{
			["getTime",objNull,[_x]] call LR_fnc_trackTime;

			_preForm = ["<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br />Team 1: %8/%15 - %1<br />Team 2: %9/%15 - %2<br />Team 3: %10/%15 - %3<br />Team 4: %11/%15 - %4<br />Team 5: %12/%15 - %5<br />Team 6: %13/%15 - %6<br />Team 7: %14/%15 - %7"];

			_timeBuffer = _x getVariable ["rawTimeBuffer",["An error occured!"]];
			_preForm append _timeBuffer;

			_formString = format _preForm;

			_msg = format ["formattedCOV: %1, at %2",_formString,_x];		// debug
			loglog(_msg);							// debug

			_varName = format ["formattedCOV_%1",_x];
			missionNamespace setVariable [_varName,_formString,true];
		} forEach _players;
	};
	case "start": {
		{
			_reg = _x getVariable ["registered",false];
			if (_reg) exitWith {
				[_thisList,"broadcast"] call LR_fnc_handleClientTrigger;
			};
//			_formString = format ["<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br />Team 1: %8/%15 - %1<br />Team 2: %9/%15 - %2<br />Team 3: %10/%15 - %3<br />Team 4: %11/%15 - %4<br />Team 5: %12/%15 - %5<br />Team 6: %13/%15 - %6<br />Team 7: %14/%15 - %7",
//			"00:00:00","00:00:00","00:00:00","00:00:00","00:00:00","00:00:00","00:00:00",1,1,1,1,1,1,1,totalLaps];

			_formString = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br />Please pass a checkpoint to update the time standings.";

			_varName = format ["formattedCOV_%1",_x];
			missionNamespace setVariable [_varName,_formString,true];

			_msg = format ["formattedCOV: %1, at %2",_formString,_x];		// debug
			loglog(_msg);							// debug

			_x setVariable ["registered",true];
		} forEach _players;
	};
};