/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Handles display things for overseers...
*
*	Parameters:
*		0: Mode (STRING)
*		1: Affected player (OBJECT)
*		2: Client ID (NUMBER)
*		3: Team for Overview (Object)
*
*	Return Value:
*		Nothing.
*/
#define SCRIPT fn_handleClientTrigger
#include "..\..\common.hpp"
#include "macros.hpp"

params [
	["_mode","clr",[""]],
	["_player",objNull,[objNull]],
	["_clientID",0,[0]],
	["_team",objNull,[objNull]]
];

_mode = toLower _mode;

switch (_mode) do {

	case "cov": {
		_player setVariable ["displayLoopOS","completeOS",true];

		while { ( (_player getVariable ["displayLoopOS","clearOS"]) isEqualTo "completeOS" ) } do {

			_msg = format ["displayLoop: %1",(_player getVariable "displayLoopOS")];
			loglog(_msg);

			_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br />Team 1: %8/%15 - %1<br />Team 2: %9/%15 - %2<br />Team 3: %10/%15 - %3<br />Team 4: %11/%15 - %4<br />Team 5: %12/%15 - %5<br />Team 6: %13/%15 - %6<br />Team 7: %14/%15 - %7";

			GETVAR(team_1);
			GETVAR(team_2);
			GETVAR(team_3);
			GETVAR(team_4);
			GETVAR(team_5);
			GETVAR(team_6);
			GETVAR(team_7);

			_formString = format [_body,_team_1,_team_2,_team_3,_team_4,_team_5,_team_6,_team_7,_lap_team_1,_lap_team_2,_lap_team_3,_lap_team_4,_lap_team_5,_lap_team_6,_lap_team_7,totalLaps];
		
			[_formString] remoteExec ["LR_fnc_hint",_clientID];
		};
	};

	case "tov": {

		_varName = format ["singleOS_%1",_team];
		_player setVariable ["displayLoopOS",_varName,true];

		_msg = format ["varName: %1",_varName];
		loglog(_msg);

		while { _varName = format ["singleOS_%1",_team]; _msg = format ["varName: %1",_varName]; loglog(_msg); ( (_player getVariable ["displayLoopOS","clearOS"]) isEqualTo _varName ) } do {

			_msg = format ["displayLoop: %1",(_player getVariable "displayLoopOS")];
			loglog(_msg);
		
			_varName = format ["raceCompleted_%1",_team];
			_completed = missionNamespace getVariable [_varName,false];
		
//			_msg = format ["Race Completed %1 | %2",_team,_completed];
//			loglog(_msg);

//			if (_completed isEqualTo true) then {
//				_body = "<t color='#ff0000' size='2.5' align='center'>Race Completed</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Race completed after: %9</t>";
//			} else {
//				_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Current Time: %2<br /><br />Current Lap: %3/%4<br />Checkpoint Alpha: %5<br />Checkpoint Bravo: %6<br />Checkpoint Charlie: %7<br />Checkpoint Delta: %8</t>";
//			};

			_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Current Time: %2<br /><br />Current Lap: %3/%4<br />Checkpoint Alpha: %5<br />Checkpoint Bravo: %6<br />Checkpoint Charlie: %7<br />Checkpoint Delta: %8</t>";
		
//			_msg = format ["Message body %1",_body];
//			loglog(_msg);

			_varName = format ["StartTime_%1",_team];
			_sTime = missionnamespace getVariable [_varName,0];
		
			_varName = format ["lap_%1",_team];
			_sLap = missionnamespace getVariable [_varName,0];
		
			_sID = _team call LR_fnc_getTeamID;
		
			_varR = format["registered_%1",_team];
			_reg = missionNamespace getVariable [_varR,false];
		
			_dTime = _sTime call LR_fnc_deltaTime;
			_dTime = _dTime call LR_fnc_convertTime;
		
			if ((_sTime == 0) || (not _reg)) then { _dTime = "Not started."; };
		
			CPVAR(cp_alpha);
			CPVAR(cp_bravo);
			CPVAR(cp_charlie);
			CPVAR(cp_delta);
		
			_varName = format  ["raceCompletedTime_%1",_team];
			_rcT= missionnamespace getVariable [_varName,0];
		
			_formString = format [_body,(_sID + 1),_dTime,_sLap,totalLaps,_cp_alpha,_cp_bravo,_cp_charlie,_cp_delta,_rcT];
		
//			_msg = format ["Composed Text: %1",_formString];
//			loglog(_msg);
//			_msg = format ["ownerID b4 rExec: %1",_clientID];
//			loglog(_msg);
			
			[_formString] remoteExec ["LR_fnc_hint",_clientID];
		};
	};

	case "clr": {
		_player setVariable ["displayLoopOS","clearOS",true];
		hintSilent "";

		_msg = format ["displayLoop: %1",(_player getVariable "displayLoopOS")];
		loglog(_msg);

		_msg = format ["Cleared"];
		loglog(_msg);
	};
};