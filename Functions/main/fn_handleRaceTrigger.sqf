/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Handles the triggers activations for the race.
*
*	Parameters:
*		0: List of objects in trigger (ARRAY)
*		1: Mode Setting (STRING)
*/
#define SCRIPT fn_handleRaceTrigger
#include "..\..\common.hpp"

//run_servercheck;

params [
	["_list",[],[[]]],
	["_mode","start",[""]],
	["_args",[],[[]]]
];

_teams = [team_1,team_2,team_3,team_4,team_5,team_6,team_7];
_msg = format ["List: %1",_list];
loglog(_msg);


{
	if (!(_x in _list)) then {
		_teams = _teams - [_x];
	} else {
		_msg = format ["%1",_x];
		loglog(_msg);
	};
} forEach _teams;

_msg = format ["Teams: %1",_teams];		// debug
loglog(_msg);							// debug

_mode = toLower _mode;

switch (_mode) do {
	case "start": {
		_registered 	= [];
		_Nregistered	= [];
		{
			_tVarName 	= format ["registered_%1",_x];
			_tVar		= missionnamespace getVariable [_tVarName,false];

			_msg = format["tVar: %1",_tVar];			// debug
			loglog(_msg);								// debug
			_msg = format["_tVarName: %1",_tVarName];	// debug
			loglog(_msg);								// debug

			if (_tVar) then {
				_registered 	pushBack _x;

				_msg = format["_x reg: %1",_x];			// debug
				loglog(_msg);							// debug

			} else {
				_Nregistered 	pushBack _x;

				_msg = format["_x nReg: %1",_x];		// debug
				loglog(_msg);							// debug
			};

		} forEach _teams;
		
		{
			_msg = format["_x cFunc: %1",_x];			// debug
			loglog(_msg);								// debug

			["registerTeam",_x] call LR_fnc_trackTime;
		} forEach _Nregistered;

		{
			_varName = format ["lap_%1",_x];
			_laps	 = missionnamespace getVariable [_varName,0];
			["lap",_x,[_laps]] call LR_fnc_trackTime;
		} forEach _registered;

	};

	case "checkpoint": {

		if ((count _args) > 0) then {
			_args params  [
				["_checkpoint","cp_alpha",[""]]
			];
		};

		_registered 	= [];
		{
			_tVarName 	= format ["registered_%1",_x];
			_tVar		= missionnamespace getVariable [_tVarName,false];

			if (_tVar) then {
				_registered 	pushBack _x;

				_msg = format["_x reg: %1",_x];			// debug
				loglog(_msg);							// debug
			};

		} forEach _teams;

		{
			["checkpoint",_x,[_checkpoint]] call LR_fnc_trackTime;
		} forEach _registered;
	};
};