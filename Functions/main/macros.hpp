//Fancy defines
#define GETVAR(var1) \
	_var = format["StartTime_%1",##var1##];\
	_varR = format["registered_%1",##var1##];\
	_time = missionNamespace getVariable [_var,0];\
	_reg = missionNamespace getVariable [_varR,false];\
	_##var1## = _time call LR_fnc_deltaTime;\
	_##var1## = _##var1## call LR_fnc_convertTime;\
	_varName = format ["lap_%1",##var1##];\
	_lap_##var1## = missionnamespace getVariable [_varName,0];\
	if ((_time == 0) || (not _reg)) then { _##var1## = "Not started."; }

#define CPVAR(var1) \
	_varName = format ["lap%1_%2_passTime_%3",_sLap, #var1 ,_team];\
	_temp = missionnamespace getVariable [_varName,0];\
	_##var1## = _temp call LR_fnc_convertTime;\
	if (_temp == 0) then { _##var1## = "Checkpoint not passed."; }


#define NULL 	_##var1## = _temp call LR_fnc_deltaTime;\
	_msg = format ["Varname cpvar: %1",_varName];\
	loglog(_msg);\
	_msg = format ["Checkpoint: %2 PassTime: %1",_temp, #var1 ];\
	loglog(_msg);\