/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Handles various display stuff for clients.
*
*	Parameters:
*		0: addAction array (ARRAY)
*		1: Mode (STRING)
*		2: Client ID (NUMBER)
*		3: Team to display stats for (OBJECT)
*
*	Return Value:
*		Nothing.
*/
#define SCRIPT fn_handlesDisplay
#include "..\..\common.hpp"
#include "macros.hpp"


params [
	["_player",objNull,[objNull]],
	["_mode","singleov",[""]],
	["_clientID",0,[0]]
];

_team = _player getVariable ["assignedTeam",objNull];

_msg = format ["Assigned Team: %1",_team];
loglog(_msg);

_msg = format ["ownerID: %1",_clientID];
loglog(_msg);

_mode = toLower _mode;

switch (_mode) do {
	case "singleov": {

		_player setVariable ["displayLoop","singleov",true];
	
		while { ( (_player getVariable ["displayLoop","clear"]) isEqualTo "singleov" ) } do {

			_varName = format ["raceCompleted_%1",_team];
			_completed = missionNamespace getVariable [_varName,false];

//			_msg = format ["Race Completed %1 | %2",_team,_completed];
//			loglog(_msg);

//			if (_completed isEqualTo true) then {
//				_body = "<t color='#ff0000' size='2.5' align='center'>Race Completed</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Race completed after: %9</t>";
//			} else {
//				_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Current Time: %2<br /><br />Current Lap: %3/%4<br />Checkpoint Alpha: %5<br />Checkpoint Bravo: %6<br />Checkpoint Charlie: %7<br />Checkpoint Delta: %8</t>";
//			};

			_body = "<t color='#ff0000' size='2.5' align='center'>Time Overview</t><br />-------------------------------<br /><br /><t color='#ffff00' size='1.5' align='center'>Team %1</t><br /><br /><t align ='left'>Current Time: %2<br /><br />Current Lap: %3/%4<br />Checkpoint Alpha: %5<br />Checkpoint Bravo: %6<br />Checkpoint Charlie: %7<br />Checkpoint Delta: %8</t>";

//			_msg = format ["Message body %1",_body];
//			loglog(_msg);

			_varName = format ["StartTime_%1",_team];
			_sTime = missionnamespace getVariable [_varName,0];

			_varName = format ["lap_%1",_team];
			_sLap = missionnamespace getVariable [_varName,0];

			_sID = _team call LR_fnc_getTeamID;

			_varR = format["registered_%1",_team];
			_reg = missionNamespace getVariable [_varR,false];

			_dTime = _sTime call LR_fnc_deltaTime;
			_dTime = _dTime call LR_fnc_convertTime;

			if ((_sTime == 0) || (not _reg)) then { _dTime = "Not started."; };

			CPVAR(cp_alpha);
			CPVAR(cp_bravo);
			CPVAR(cp_charlie);
			CPVAR(cp_delta);

			_varName = format  ["raceCompletedTime_%1",_team];
			_rcT= missionnamespace getVariable [_varName,0];

			_formString = format [_body,(_sID + 1),_dTime,_sLap,totalLaps,_cp_alpha,_cp_bravo,_cp_charlie,_cp_delta,_rcT];

//			_msg = format ["Composed Text: %1",_formString];
//			loglog(_msg);
//			_msg = format ["ownerID b4 rExec: %1",_clientID];
//			loglog(_msg);
			
			[_formString] remoteExec ["LR_fnc_hint",_clientID];
		};
	};
	case "completeov": {

		_player setVariable ["displayLoop","completeov",true];

		while { ( (_player getVariable ["displayLoop","clear"]) isEqualTo "completeov" ) } do {

			_varName = format ["formattedCOV_%1",_player];
			_msg = missionNamespace getVariable [_varName,"Race has not been started."];

			loglog(_msg);

			[_msg] remoteExec ["LR_fnc_hint",_clientID];

			sleep 5;
		};
	};
	case "curstand": {
		
		_player setVariable ["displayLoop","curstand",true];

		while { ( (_player getVariable ["displayLoop","clear"]) isEqualTo "curstand" ) } do {
			
			_varName = format ["formattedCS_%1",_player];
			_msg = missionNamespace getVariable [_varName,"Race has not been started."];

			[_msg] remoteExec ["LR_fnc_hint",_clientID];
			sleep 5;
		};
	};
	case "clear": {

		_player setVariable ["displayLoop","clear"];
		hintSilent "";
	};
	default {
		_msg = format ["Invalid mode parameter supplied: %1. Should be singleov, completeov, curstand, or clear.",_mode];
		logerror(_msg);
	};
};