/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Adds teleport and overview actions to laptops.
*
*	Parameters:
*		0: Current Location (STRING)
*		1: Laptop Object (OBJECT)
*/
#define SCRIPT fn_handleRaceTrigger
#include "..\..\common.hpp"

params [
	["_location","pit_stop",[""]],
	["_laptop",objNull,[objNull]]
];

_locations = ["sp_1","sp_2","sp_3","sp_4","pit_stop"];

_actionPool = [
	["<t color='#005555'>Teleport to Service Point 1</t>",		{ ["sp_1",(_this select 1)] 			remoteExec ["LR_fnc_teleport",2]; },	nil,5],
	["<t color='#005555'>Teleport to Service Point 2</t>",		{ ["sp_2",(_this select 1)] 			remoteExec ["LR_fnc_teleport",2]; },	nil,5],
	["<t color='#005555'>Teleport to Service Point 3</t>",		{ ["sp_3",(_this select 1)] 			remoteExec ["LR_fnc_teleport",2]; },	nil,5],
	["<t color='#005555'>Teleport to Service Point 4</t>",		{ ["sp_4",(_this select 1)] 			remoteExec ["LR_fnc_teleport",2]; },	nil,5],
	["<t color='#0000ff'>Teleport to Pit Stop</t>",				{ ["pit_stop",(_this select 1)] 		remoteExec ["LR_fnc_teleport",2]; },	nil,5]
];

_actions = [];

{
	if (not (_location == _x)) then {
		_actions pushBack (_actionPool select _forEachIndex);
	};
} forEach _locations;

{
	_laptop addAction _x;
} forEach _actions;