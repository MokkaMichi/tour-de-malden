/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Sends a message to all passengers in the car.
*
*	Parameters:
*		0: Vehicle (OBJECT)
*		1: Message to send (STRING)
*
*	Return Values:
*		Nothing.
*/

#define SCRIPT fn_handleRaceTrigger
#include "..\..\common.hpp"

params [
	["_car",objNull,[objnull]],
	["_msg","",[""]]
];

_passengers = crew _car;
_passengers = _passengers arrayIntersect (playableUnits);

{
	[_msg] remoteExec ["systemChat",_x];
} forEach _passengers;