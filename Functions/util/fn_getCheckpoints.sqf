/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Returns the last passed checkpoint, or all passed checkpoints.
*
*	Parameters:
*		0: Team Object (OBJECT)
*		1: Mode (NUMBER) | 0: All Checkpoints; 1: Last Checkpoint
*
*	Return Values: 
*		0: Current Checkpoint (STRING) // All Checkpoints (ARRAY)
*		1: Current Lap (NUMBER)
*/
#define SCRIPT fn_trackTime
#include "..\..\common.hpp"

//run_servercheck;

params [
	["_team",objNull,[objNull]],
	["_mode",0,[0]]
];

if (_mode == 0) then {
	_return = [];
} else {
	_return = ["start",0];
};

for "_i" from 0 to totalLaps step 1 do {
	{
		_varName = format ["lap%1_%2_passTime_%3",_i,_x,_team];
		_cpTime = missionnamespace getVariable [_varName,0];

		if (_cpTime > 0) then {
			if (_mode == 0) then {
				_return pushBack format ["%1_%2",_x,_i];
			} else {
				_return = [_x,_i];
			};
		};
	} forEach checkpoints;
};

_return;