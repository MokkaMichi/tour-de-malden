/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Calculates delta-time for given start-time.
*
*	Parameters:
*		0: Start Time
*/
#define SCRIPT fn_deltaTime
#include "..\..\common.hpp"

params [
	["_dStart",0,[0]],
	["_dEnd",nil,[0]]
];

if (isNil "_dEnd") then {
	_dEnd = serverTime;
};

_delta = _dEnd - _dStart;
_delta;