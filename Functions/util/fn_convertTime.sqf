/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Converts time from seconds to hh:mm:ss:ms.
*
*	Parameters:
*		0: Time in seconds (NUMBER)
*
*	Return Values:
*		0: Converted time (STRING)
*/
#define SCRIPT fn_convertTime
#include "..\..\common.hpp"

params [
	["_tis",0,[0]]
];

_template = "%3:%2:%1";

_tis = [_tis, 2] call BIS_fnc_cutDecimals;

_tim = floor (_tis / 60);
_tih = floor (_tim / 60);


_tis = _tis - _tim * 60;
_tis = _tis - _tih * 3600;


if (_tis < 10) then {
	_tis = format ["0%1",_tis];
};
if (_tim < 10) then {
	_tim = format ["0%1",_tim];
};
if (_tih < 10) then {
	_tih = format ["0%1",_tih];
};


_converted = format [_template,_tis,_tim,_tih];
_converted;