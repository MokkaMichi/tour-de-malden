/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Returns the correct ID of the given checkpoint.
*
*	Parameters:
*		0: Checkpoint reference (STRING)
*		1: Mode Setting (STRING)
*
*	Return Value:
*		0: Checkpoint ID (String)
*/
#define SCRIPT fn_getCheckpointID
#include "..\..\common.hpp"

//run_servercheck;

params [
	["_cpRaw","",[""]],
	["_mode","nato",[""]]
];

_mode = toLower _mode;

_return = "";

switch (_mode) do {
	case "nato": 	{
		_sel = ["Alpha","Bravo","Charlie","Delta"];
	};
	case "roman": 	{
		_sel = ["I","II","III","IV"];
	};
	case "numeric":	{
		_sel = ["1","2","3","4"];
	};
	case "word":	{
		_sel = ["One","Two","Three","Four"];
	};
	default 		{
		_sel = ["Alpha","Bravo","Charlie","Delta"];
	};
};

_index = checkPoints find _cpRaw;
if (not (_index < 0)) then {
	_return = _sel select _index;
};

_return;