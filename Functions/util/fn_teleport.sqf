/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Teleports players to given location
*
*	Parameters:
*		0: Destination (STRING)
*		1: Player (OBJECT)
*/
#define SCRIPT fn_handleRaceTrigger
#include "..\..\common.hpp"

params [
	["_location","pit_stop",[""]],
	["_player",objNull,[objNull]]
];

_pos = getMarkerPos _location;

_pos = [(_pos select 0),(_pos select 1),0];

_player setPos _pos;