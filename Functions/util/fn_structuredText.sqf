/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*       Formats supplied input into structured text.
*
*	Parameters:
*		0: Text to be formatted (STRING)
*		1: Size of the text (NUMBER)
*		2: Font of the text (STRING/CfgFontFamilies)
*		3: Colour of the text (STRING/COLOUR)
*		4: Shadow of the text (NUMBER)
*		5: Colour of the shadow (STRING/COLOUR)
*
*	Return Values:
*		0: Input formatted as structured text (TEXT)
*/

params [
	["_text","",[""]],
	["_size",1,[0]],
	["_font","PuristaLight",[""]],
	["_colour","#ffffff",[""]],
	["_shadow",0,[0]],
	["_shadowColour","#000000",[""]]	
];

if(_shadow > 0) then {
	_text = format ["<t size='%2' font='%3' color='%4' shadow='%5' shadowColor='%6'>%1</t>",_text,_size,_font,_colour,_shadow,_shadowColour];
} else {
	_text = format ["<t size='%2' font='%3' color='%4'>%1</t>",_text,_size,_font,_colour];
};

_text = parsetext _text;

_text;