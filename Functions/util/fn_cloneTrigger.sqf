/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Clones a trigger.
*
*	Parameters:
*		0: Trigger to Clone (TRIGGER)
*		1: Custom Statement (SCRIPT)
*		2: Make clone public (BOOL)
*
*	Return Values:
*		0: Cloned Trigger (TRIGGER)
*/
#define SCRIPT fn_cloneTrigger
#include "..\..\common.hpp"

params [
	["_orig"],
	["_statement",[],[[]],[3]],
	["_activation",[],[[]],[3]],
	["_public",false,[false]]
];


_pos 		= getPos 			_orig;
_area		= triggerArea 		_orig;
_activation	= triggerActivation _orig;

// Create the trigger and set the according parameters
_trg = 	createTrigger ["EmptyDetector", _pos, _public];
_trg 	setTriggerArea _area;
_trg	setTriggerActivation _activation;
_trg	setTriggerStatements _statement;

_trg;