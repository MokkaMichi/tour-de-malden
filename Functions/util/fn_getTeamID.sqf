/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Returns the ID of the given team car.
*
*	Parameters:
*		0: Team Object (OBJECT)
*
*	Return Values:
*		0: Team ID (SCALAR)
*/

#define SCRIPT fn_getTeamID
#include "..\..\common.hpp"

//run_servercheck;

params [
	["_team",objNull,[objNull]]
];

//_varDump = format["_team: %1",_team];
//loginfo(_varDump);

_team = vehicleVarName _team;
_id = -1;
switch (_team) do {
	case "team_1": { _id = 0; };
	case "team_2": { _id = 1; };
	case "team_3": { _id = 2; };
	case "team_4": { _id = 3; };
	case "team_5": { _id = 4; };
	case "team_6": { _id = 5; };
	case "team_7": { _id = 6; };
};

//_varDump = format["_id: %1",_id];
//loginfo(_varDump);


_id;