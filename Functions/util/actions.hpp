// Stores action definitions for fn_addPassengerActions

#define SINGLEOV \
"<t color='#ff5500'>Team Overview</t>",\
{ [(_this select 0),"singleov",clientOwner] remoteExec ["LR_fnc_handleDisplay",2]; },\
nil,\
5

#define COMPLETEOV \
"<t color='#ff5500'>Complete Overview</t>",\
{ [(_this select 0),"completeov",clientOwner] remoteExec ["LR_fnc_handleDisplay",2]; },\
nil,\
4

#define CURSTAND \
"<t color='#ff5500'>Current Standing</t>",\
{ [(_this select 0),"curstand",clientOwner] remoteExec ["LR_fnc_handleDisplay",2]; },\
nil,\
3

#define CLEAR \
"<t color='#ff0000'>Clear Overviews</t>",\
{ [(_this select 0),"clear",clientOwner] remoteExec ["LR_fnc_handleDisplay",2]; },\
nil,\
2