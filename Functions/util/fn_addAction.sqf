/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Handles adding actions and stores the action ID
*
*	Parameters:
*		0: Player (OBJECT)
*		1: Code (OBJECT)
*
*	Return Value:
*		Nothing.
*/
#define SCRIPT fn_addAction
#include "..\..\common.hpp"



params [
	["_player",objNull,[objNull]],
	["_code",[],[[]]]
];

_id = _player addAction _code;

_actions = _player getVariable ["passengerActions",[]];
_actions pushBack _id;
_player setVariable ["passengerActions",_actions,true];