/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Adds actions for overseers:
*			- Complete, realtime, standing
*			- Realtime, team-specific overview for every competing team
*		Requires trait "mokka_overseer". remoteExec ["d",2];  from initPlayerLocal
*		
*		This functions is a bit clunky and dirty, but oh well...
*
*	Parameters:
*		0: Player (OBJECT)
*		1: Client ID (NUMBER)
*
*	Return Values:
*		Nothing.
*/
#define SCRIPT fn_addOverseerActions
#include "..\..\common.hpp"

params [
	["_player",objNull,[objNull]],
	["_clientID",0,[0]]
];

_msg = format ["Params dump: %1 | %2",_player,_clientID];
loglog(_msg);


_actions = [
	["<t color='#00ff00'>Complete Overview</t>",		{ ["cov",player,clientOwner] 			remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,5],
	["<t color='#ff5500'>Team Overview (Team 1)</t>",	{ ["tov",player,clientOwner,team_1] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#ff5500'>Team Overview (Team 2)</t>",	{ ["tov",player,clientOwner,team_2] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#ff5500'>Team Overview (Team 3)</t>",	{ ["tov",player,clientOwner,team_3] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#ff5500'>Team Overview (Team 4)</t>",	{ ["tov",player,clientOwner,team_4] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#ff5500'>Team Overview (Team 5)</t>",	{ ["tov",player,clientOwner,team_5] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#ff5500'>Team Overview (Team 6)</t>",	{ ["tov",player,clientOwner,team_6] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#ff5500'>Team Overview (Team 7)</t>",	{ ["tov",player,clientOwner,team_7] 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,4],
	["<t color='#0000ff'>Clear Overview</t>",			{ ["clr",player]					 	remoteExec ["LR_fnc_handleDisplayOS",2]; },	nil,3]
];

_msg = format ["Actions: %1",_actions];
loglog(_msg);

_player setVariable ["displayLoop","clearOS",true];

{
	[_player,_x] remoteExec ["LR_fnc_addAction",_clientID];
	_msg = format ["Adding action: %1",_x];
	loglog(_msg);
} forEach _actions;

