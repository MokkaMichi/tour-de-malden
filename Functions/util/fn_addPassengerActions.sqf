/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Adds actions to the competitors in the team cars.
*
*	Parameters:
*		0: Player (OBJECT)
*		1: Position in vehicle (STRING)
*		2: Vehicle (OBJECT)
*		3: Client ID (NUMBER)
*
*	Return Value:
*		Nothing.
*/
#define SCRIPT fn_addPassengerActions
#include "..\..\common.hpp"
#include "actions.hpp"

//run_servercheck;

params [
	["_player",objNull,[objNull]],
	["_position","driver",[""]],
	["_vehicle",objNull,[objNull]],
	["_ID",0,[0]]
];

_teams = [team_1,team_2,team_3,team_4,team_5,team_6,team_7];	// Make this nicer sometime in the future. Maybe...


_msg = format ["params dump: %1 | %2 | %3 | %4",_ID,_player,_position,_vehicle];
loglog(_msg);

if (not (_vehicle in _teams)) exitWith {
	_msg = format ["Invalid vehicle supplied to function: %1 by player %2",_vehicle,_player];
	logwarning(_msg);
};

_player setVariable ["displayLoop","clear",true];
_player setVariable ["assignedTeam",_vehicle,true];

{
	[_player,_x] remoteExec ["LR_fnc_addAction",_ID];

	_msg = format ["Full array: %1",_x];
	loglog(_msg);
} forEach [[SINGLEOV],[COMPLETEOV],/*[CURSTAND],*/[CLEAR]];

[_player, ["GetOutMan",{
	_msg = format ["EH Data Array GOM: %1",_this];
	loglog(_msg);

	_this params [
		["_player",objNull,[objNull]],
		["_position","driver",[""]],
		["_vehicle",objNull,[objNull]],
		["_null",[]]
	];

	_msg = format ["params dump: %1 | %2 | %3 | %4",_player,_position,_vehicle,clientOwner];
	loglog(_msg);

	_actions = _player getVariable ["passengerActions",[]];

	{
		_player removeAction _x;
	} forEach _actions;

	_player setVariable ["displayLoop","clear"];
	_player setVariable ["assignedTeam",objNull,true];
	_player setVariable ["passengerActions",[]];

	_player removeAllEventHandlers "GetOutMan";
}]] remoteExec ["addEventHandler",_ID];