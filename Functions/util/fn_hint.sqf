/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		hintSilent stuff, because remoteExec hates me ._.
*
*	Parameters:
*		0: Message (STRING)
*
*	Return Value:
*		Nothing.
*/
#define SCRIPT fn_hint
#include "..\..\common.hpp"


params [
	["_msg","",[""]]
];

hintSilent parseText _msg;