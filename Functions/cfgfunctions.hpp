class LR {
    tag = "LR";
    class main {
        file = "Functions\main";
        class handleRaceTrigger {};
        class handleClientTrigger {};
        class trackTime {};
        class handleDisplay {};
        class handleDisplayOS {};
    };
    class util {
        file = "Functions\util";
        class cloneTrigger {};
        class deltaTime {};
        class convertTime {};
        class getTeamID {};
        class getCheckpoints {};
        class structuredText {};
        class sendTeamMessage {};
        class getCheckpointID {};
        class addPassengerActions {};
        class hint {};
        class addAction {};
        class addOverseerActions {};
        class addLaptopActions {};
        class teleport {};
    };
};