/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Dispenses wheels.
*
*	Parameters:
*		0: Box (OBJECT)
*
*	Return Values:
*		Nothing.
*/
#define SCRIPT wheel_dispenser_dispense
#include "..\..\common.hpp"
#include "actions.hpp"

_fnc_relPos = {
	_pos = (getPosATL (_this select 0)) vectorAdd (_this select 1);
	_pos;
};

params [
	["_box",objNull],
	["_caller",objNull]
];

_index = _box getVariable ["index",0];

if (_box getVariable "wheels_dispensed") exitWith {
	_msg = "The wheels have already been dispensed. Please reset the dispenser first, before trying to reuse it!";
	_msg remoteExec ["systemChat",_caller];
};


_wheel_1 = [
	[
		0.146,
		1.944,
		0.1
	],
	[
		0.245,
		-2.712,
		0.1
	],
	[
		2.981,
		0.659,
		0.2
	],
	[]
];
	
_wheel_2 = [
	[
		-2.632,
		2.075,
		0.1
	],
	[
		1.91,
		-4.028,
		0.1
	],
	[
		3.841,
		2.755,
		0.2
	],
	[]
];
	
_wheel_3 = [
	[
		-0.206,
		-1.925,
		0.013
	],
	[
		2.712,
		0.690,
		0.1
	],
	[
		-1.498,
		2.330,
		0.2
	],
	[]
];
	
_wheel_4 = [
	[
		-2.838,
		-1.794,
		0.019
	],
	[
		4.297,
		-0.773,
		0.1
	],
	[
		-0.751,
		4.411,
		0.2
	],
	[]
];

_wheels = [];

{
	_pos = (getPos _box) vectorAdd _x;
	_wheel = "ACE_wheel" createVehicle _pos;
	_wheel setPos _pos;
	_wheels pushBack _wheel;

} forEach [(_wheel_1 select _index),(_wheel_2 select _index),(_wheel_3 select _index),(_wheel_4 select _index)];

