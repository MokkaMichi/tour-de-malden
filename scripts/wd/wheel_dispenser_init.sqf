/*
*	Author: Mokka (lastresortgaming.net)
*
*	Description:
*		Initialises the wheel dispenser boxes.
*
*	Parameters:
*		0: Box (OBJECT)
*
*	Return Values:
*		Nothing.
*/
#define SCRIPT wheel_dispenser_init
#include "..\..\common.hpp"
#include "actions.hpp"

params [
	["_box",objNull],
	["_index",0,[0]]
];

_box setVariable ["index",_index,true];

_box addAction DISPENSE;
_box setVariable ["wheels_dispensed",false,true];