#include "common.hpp"
#define SCRIPT initPlayerLocal

waitUntil {(vehicle player == player) && (not (isNull player))};

_player = player;

enableSaving false;
enableSentences false;
adminCurators = allCurators;

//------------------- client executions

if (hasInterface) then {

_null = [] execVM "scripts\pilotCheck.sqf"; 									// pilots only
player setVariable ["tf_sendingDistanceMultiplicator", 1.5];


player addEventHandler ["Fired", {
	params ["_unit", "_weapon", "", "", "", "", "_projectile"];
	if (_unit distance2D (getMarkerPos "respawn_west") < 150) then {
		deleteVehicle _projectile;
		hintC "Are you some kind of special? No using Weapon Systems at base! I've had to write this because of retards like you!";
	}}];
	
};


switch (true) do {
	case (player getUnitTrait "mokka_engineer"): 	{
		[player, "engineer_respawn", "Respawn (Engineers)"] call BIS_fnc_addRespawnPosition;
	};
	case (player getUnitTrait "mokka_pilot"): 		{
		[player, "pilot_respawn", "Respawn (Pilots)"] call BIS_fnc_addRespawnPosition;
	};
	default 										{
		[player, "main_respawn", "Respawn (Main)"] call BIS_fnc_addRespawnPosition;
	};
};

player addEventHandler ["GetInMan", {

	_msg = format ["EH GIM Data Array: %1",_this];
	loglog(_msg);
	_msg = format ["clientOwner: %1",clientOwner];
	loglog(_msg);

	_this params [
		["_player",objNull,[objNull]],
		["_position","driver",[""]],
		["_vehicle",objNull,[objNull]],
		["_null",[]]
	];

	_msg = format ["params dump: %1 | %2 | %3 | %4",_player,_position,_vehicle,clientOwner];
	loglog(_msg);

	_player setVariable ["displayLoop","clear"];

	_player setVariable ["assignedTeam",_vehicle,true];

	[[_player,_position,_vehicle,clientOwner],LR_fnc_addPassengerActions] remoteExec ["call",2];
}];

_overseer = player getUnitTrait "mokka_overseer";
_msg = format ["Trait Overseer: %1",_overseer];
loglog(_msg);

if (_overseer) then {
	[_player,clientOwner] remoteExec ["LR_fnc_addOverseerActions",2];
	_msg = format ["Executing LR_fnc_addOverseerActions on client %1",clientOwner];
	loglog(_msg);
};