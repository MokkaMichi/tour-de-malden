#define logerror(msg) \
    diag_log format ["[MaldenRace] (ERROR) %1: %2", #SCRIPT, msg]
//  systemchat format ["[MaldenRace] (ERROR) %1: %2", #SCRIPT, msg]

#define logwarning(msg) \
    diag_log format ["[MaldenRace] (WARNING) %1: %2", #SCRIPT, msg]
//  systemchat format ["[MaldenRace] (WARNING) %1: %2", #SCRIPT, msg]

#define loglog(msg) \
    diag_log format ["[MaldenRace] (LOG) %1: %2", #SCRIPT, msg]
//  systemchat format ["[MaldenRace] (LOG) %1: %2", #SCRIPT, msg]

#define loginfo(msg) \
    diag_log format ["[MaldenRace] (INFO) %1: %2", #SCRIPT, msg]
//  systemchat format ["[MaldenRace] (INFO) %1: %2", #SCRIPT, msg]

#define logserverror \
    logwarning("This function may only be executed on the server!")

#define logclierror \
    logwarning("This function may only be executed on the client!")

#define run_servercheck \
    if (not isServer) exitWith {\
        logserverror;\
    }

#define run_clientcheck \
    if (not hasInterface) exitWith {\
        logclierror;\
    }